/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    
    //Setting the default midi output to SimpleSynth
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
    audioDeviceManager.initialiseWithDefaultDevices(2, 2);
    audioDeviceManager.addAudioCallback(this);
    
    amplitudeSlider.setSliderStyle(Slider::LinearHorizontal);
    amplitudeSlider.setRange(0, 1);
    addAndMakeVisible(amplitudeSlider);
    amplitudeSlider.addListener(this);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this);
}

void MainComponent::resized()
{
    amplitudeSlider.setBounds(10, 10, 200, 20);

}
void MainComponent::audioDeviceIOCallback(const float **inputChannelData, int numInputChannels, float **outputChannelData, int numOutputChannels, int numSamples)
{
    DBG ("Audio Device IO Callback\n");
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    while(numSamples--)
    {
        *outL = *inL * amplitude;
        *outR = *inR * amplitude;
        inL++;
        outL++;
        outR++;
    }
}
void MainComponent::audioDeviceAboutToStart(AudioIODevice* device)
{
    DBG ("Audio Device About To Start\n");
    //DBG ("Sample Rate:" << AudioIODevice::getCurrentSampleRate() << "\n");
}
void MainComponent::audioDeviceStopped()
{
    DBG ("Audio Device Stopped\n");
}
void MainComponent::sliderValueChanged(Slider* slider)
{
    amplitude = amplitudeSlider.getValue();
}
void MainComponent::handleIncomingMidiMessage(MidiInput*, const MidiMessage& message_)
{
    if (message_.getControllerNumber() == 1)
    {
        amplitude = message_.getControllerValue()/127.0;
        amplitudeSlider.getValueObject().setValue(amplitude);
    }
}